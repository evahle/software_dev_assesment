<?php

// Sending GET resquest
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.sightmap.com/v1/assets/1273/multifamily/units?per-page=500',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'API-Key: 7d64ca3869544c469c3e7a586921ba37'
  ),
));

$response = curl_exec($curl);
curl_close($curl);


// Turning response into JSON
$paging_units = json_decode($response, true);


//Dropping the paging information
unset($paging_units["paging"]);


//Creating array with unit numbder and area
foreach($paging_units as $units){
    foreach($units as $unit){
        $units_array [] = [
            'unit_number'=>$unit['unit_number'],
            'area'=>$unit['area'],
        ];
    }
}


// Finding units with area of 1
foreach($units_array as $unit){
    if($unit['area']==1)
        $area_one [] = [
            'unit_number'=> $unit['unit_number']. ',',
        ]
        ;
}


//Finding units with area greater than 1
foreach($units_array as $unit){
    if($unit['area']>1)
        $area_more_than_one [] = [
            'unit_number'=> $unit['unit_number']. ',',
        ]
        ;
}


//Listing units with area equal to 1
echo 'Units With Area Equal to 1';
echo "\r\n";

foreach ($area_one as $unit) {
    echo $unit['unit_number'];
    echo "\r\n";

}

//Listing units with area greater than 1
echo "\r\n";
echo 'Units With Area Greater Than 1';
echo "\r\n";

foreach ($units_array as $unit) {
    echo $unit['unit_number'];
    echo "\r\n";

}

?>